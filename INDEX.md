# Nasm

The Netwide Assembler (NASM) is an 80x86 and x86-64 assembler designed for portability and modularity. It supports a range of object file formats, including Linux and *BSD a.out, ELF, COFF, Mach-O, 16-bit and 32-bit OBJ (OMF) format, Win32 and Win64. It will also output plain binary files, Intel hex and Motorola S-Record formats. Its syntax is designed to be simple and easy to understand, similar to the syntax in the Intel Software Developer Manual with minimal complexity. It supports all currently known x86 architectural extensions, and has strong support for macros.

# Contributing

**Would you like to contribute to FreeDOS?** The programs listed here are a great place to start. Most of these do not have a maintainer anymore and need your help to make them better. Here's how to get started:

* __Maintainers__: Let us know if you'd like to take on one of these programs, and we can provide access to the source code repository here. Please use the FreeDOS package structure when you make new releases, including all executables, source code, and metadata.

* __New developers__: We can extend access for you to track issues here.

* __Translators__: Please submit to the [FD-NLS Project](https://github.com/shidel/fd-nls).

_*Make sure to check all source code licenses, especially for any code you might reuse from other projects to improve these programs. Note that not all open source licenses are the same or compatible with one another. (For example, you cannot reuse code covered under the GNU GPL in a program that uses the BSD license.)_

## NASM.LSM

<table>
<tr><td>title</td><td>Nasm</td></tr>
<tr><td>version</td><td>2.16.03</td></tr>
<tr><td>entered&nbsp;date</td><td>2024-04-17</td></tr>
<tr><td>description</td><td>The Netwide Assembler (NASM)</td></tr>
<tr><td>summary</td><td>The Netwide Assembler (NASM) is an 80x86 and x86-64 assembler designed for portability and modularity. It supports a range of object file formats, including Linux and *BSD a.out, ELF, COFF, Mach-O, 16-bit and 32-bit OBJ (OMF) format, Win32 and Win64. It will also output plain binary files, Intel hex and Motorola S-Record formats. Its syntax is designed to be simple and easy to understand, similar to the syntax in the Intel Software Developer Manual with minimal complexity. It supports all currently known x86 architectural extensions, and has strong support for macros.</td></tr>
<tr><td>keywords</td><td>nasm, asm, assembler, assembly</td></tr>
<tr><td>author</td><td>Peter Anvin and others</td></tr>
<tr><td>maintained&nbsp;by</td><td>Peter Anvin</td></tr>
<tr><td>primary&nbsp;site</td><td>http://www.nasm.us/</td></tr>
<tr><td>platforms</td><td>DOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>[Simplified (2-Clause) BSD License](LICENSE)</td></tr>
</table>
